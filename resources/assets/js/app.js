require('./bootstrap');
$(document).ready(function(){
    $('.slide-parceiros').owlCarousel({
        items: 1,
        loop:true,
        margin:0,
        singleItem:true,
        stagePadding: 0,
        nav:true,
        center:true,
        autoHeight: true,
        autoplay:true,
        autoplayTimeout:3000,
        autoplayHoverPause:true
    });

    $('.slide-botoes').owlCarousel({
        items: 1,
        loop:true,
        margin:10,
        singleItem:true,
        stagePadding: 0,
        dots:true,
        nav:false,
        center:true,
        autoHeight: true,
        autoplay:true,
        autoplayTimeout:3000,
        autoplayHoverPause:true
    });

    $("a[href='#top']").click(function() {
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    });

    $(document).on("scroll", onScroll);

    $('.anchor').click(function (e) {
        e.preventDefault();
        e.stopPropagation();

        $(document).off("scroll");
        $('.anchor').removeClass('active');
        $(this).addClass('active');

        var target = $(this).data('target');
        $target = $(target);


        $('html, body').stop().animate({
            'scrollTop': $target.offset().top - 120
        }, 500, 'swing', function () {
            window.location.hash = target;
            $(document).on("scroll", onScroll);
        });
    });
});

$(window).scroll(function() {
  var a = 0;
  var oTop = $('#counter').offset().top - window.innerHeight;
  console.log($('#counter').offset().top);
  if (a == 0 && $(window).scrollTop() > oTop) {
    $('.count').each(function() {
      var $this = $(this),
        countTo = $this.attr('data-count');
      $({
        countNum: $this.text()
      }).animate({
          countNum: countTo
        },

        {

          duration: 2000,
          easing: 'swing',
          step: function() {
            $this.text(Math.floor(this.countNum));
          },
          complete: function() {
            $this.text(this.countNum);
            //alert('finished');
          }

        });
    });
    a = 1;
  }

});

function onScroll(event) {
    var scrollTop = $(window).scrollTop();
    $('.anchor').each(function () {

        var currLink = $(this);
        var refElement = $(currLink.data('target'));
        if (currLink.data('target') !== undefined) {

            if (
                refElement.position().top <= scrollTop + 120 &&
                refElement.position().top + refElement.height() > scrollTop + 120) {
                currLink.addClass("active");
            }
            else {
                currLink.removeClass("active");
            }
        }

    });
}
$(function() {

        // Use Modernizr to detect for touch devices, 
        // which don't support autoplay and may have less bandwidth, 
        // so just give them the poster images instead
        var screenIndex = 1,
            numScreens = $('.screen').length,
            isTransitioning = false,
            transitionDur = 1000,
            BV,
            videoPlayer,
            isTouch = Modernizr.touch,
            $bigImage = $('.big-image'),
            $window = $(window);

         //Add------------------------------
         $('.wrapper').transit(
                    {'left':'-0%'},
                    1000);
        //------------------------------------

        if (!isTouch) {
            // initialize BigVideo
            BV = new $.BigVideo({forceAutoplay:isTouch});
            BV.init();
            showVideo();

            BV.getPlayer().addEvent('loadeddata', function() {
                onVideoLoaded();
            });

            // adjust image positioning so it lines up with video
            $bigImage
                .css('position','relative')
         .imagesLoaded(adjustImagePositioning);
            // and on window resize
            $window.on('resize', adjustImagePositioning);
        }

        // Next button click goes to next div
        $('#next-btn').on('click', function(e) {
            e.preventDefault();

            if (!isTransitioning) {
                next();
            }
        });

        function showVideo() {
            BV.show($('#screen-'+screenIndex).attr('data-video'),{ambient:true});
        }

        function next() {


            isTransitioning = true;

            // update video index, reset image opacity if starting over
            if (screenIndex === numScreens) {
                $bigImage.css('opacity',1);
                screenIndex = 1;
            } else {
                screenIndex++;
            }

            if (!isTouch) {
                $('#big-video-wrap').transit({'left':'-100%'},transitionDur)
            }

            (Modernizr.csstransitions)?
                $('.wrapper').transit(
                    {'left':'-'+(100*(screenIndex-1))+'%'},
                    transitionDur,
                    onTransitionComplete):
                onTransitionComplete();



        }

        function onVideoLoaded() {
            $('#screen-'+screenIndex).find('.big-image').transit({'opacity':0},500)
        }

        function onTransitionComplete() {
            isTransitioning = false;
            if (!isTouch) {
                $('#big-video-wrap').css('left',0);
                showVideo();
            }
        }

        function adjustImagePositioning() {
            $bigImage.each(function(){
                var $img = $(this),
                    img = new Image();

                img.src = $img.attr('src');

                var windowWidth = $window.width(),
                    windowHeight = $window.height(),
                    r_w = windowHeight / windowWidth,
                    i_w = img.width,
                    i_h = img.height,
                    r_i = i_h / i_w,
                    new_w, new_h, new_left, new_top;

                if( r_w > r_i ) {
                    new_h   = windowHeight;
                    new_w   = windowHeight / r_i;
                }
                else {
                    new_h   = windowWidth * r_i;
                    new_w   = windowWidth;
                }

                $img.css({
                    width   : new_w,
                    height  : new_h,
                    left    : ( windowWidth - new_w ) / 2,
                    top     : ( windowHeight - new_h ) / 2
                })

            });

        }
    });