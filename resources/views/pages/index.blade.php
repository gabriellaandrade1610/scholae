@extends('pages.layouts.layout')

@section('content')

	@include('pages.sections.slide')
	@include('pages.sections.quem-somos')
	@include('pages.sections.botoes')
	@include('pages.sections.oque-fazemos')
	@include('pages.sections.metodologia')
	@include('pages.sections.contabilizador')
	@include('pages.sections.fale-conosco')
	@include('pages.sections.parceiros')
	
@endsection