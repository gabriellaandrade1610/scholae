<section id="slide">
    <div class="overlay"></div>
    <video id="videobcg" preload="auto" autoplay="true" loop="loop" muted="muted" volume="0">
        <source src="video/video_slide.mp4" type="video/mp4">
    </video>
    <div class="container h-100">
      <div class="d-flex h-100 text-center align-items-center">
        <div class="w-100 text-white"style="margin-top: 200px">
          <h1 class="title-slide">Aprendendo Empreendendo:</h1>
          <h4 class="subtitle-slide lead mb-0">uma educação para a inovação!</h4>
          <a class="mt-3 mb-5 btn btn-outline-secondary btn-scholae anchor" data-target="#quem-somos" href="{{ route('index')}}'#quem-somos'" title="Quem somos">saiba mais</a>
        </div>
      </div>
    </div>
</section>