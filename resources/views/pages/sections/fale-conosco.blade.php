<section id="fale-conosco" class="parallax-window" data-parallax="scroll" data-image-src="/images/min/background2-opacity.png">
	<div class="d-none d-lg-block">
		<div class="d-flex align-items-center mx-5 p-5">
		  	<div class="col-md-5 mx-auto">
		  		<h1 class="mb-3">Fale Conosco</h1>
		  		<div id="map"></div>
			</div>
			<div class="col-md-5 mx-auto">
		  		<form action="{{ route('sendContact') }}" method="post" class="form-site">
		  			{{ csrf_field() }}
		  			<div class="form-group">
		  				<input class="form-control" type="text" placeholder="Digite seu nome" name="nome" required>
		  			</div>
		  			<div class="form-group">
		  				<input class="form-control" type="email" placeholder="Digite seu E-mail" name="email" required>
		  			</div>
		  			<div class="form-group">
		  				<input class="form-control" type="text" placeholder="Digite seu telefone" name="telefone" required>
		  			</div>
		  			<div class="form-group">
		  				<textarea class="form-control" placeholder="Digite sua mensagem" name="mensagem" required></textarea>
		  			</div>
		  			<div class="form-group pt-3">
		  				<button type="submit" class="btn btn-outline-secondary btn-scholae">enviar</button>
		  			</div>
		  		</form>
			</div>
		</div>
	</div>
	<div class="container d-block d-lg-none">
	  	<div class="col-xs-12 mx-auto">
	  		<h1 class="pb-3 pt-3">Fale Conosco</h1>
	  		<div id="map"></div>
		</div>
		<div class="col-xs-12 mx-auto">
	  		<form action="{{ route('sendContact') }}" method="post" class="form-site">
	  			{{ csrf_field() }}
	  			<div class="form-group">
	  				<input class="form-control" type="text" placeholder="Digite seu nome" name="nome" required>
	  			</div>
	  			<div class="form-group">
	  				<input class="form-control" type="email" placeholder="Digite seu E-mail" name="email" required>
	  			</div>
	  			<div class="form-group">
	  				<input class="form-control" type="text" placeholder="Digite seu telefone" name="telefone" required>
	  			</div>
	  			<div class="form-group">
	  				<textarea class="form-control" placeholder="Digite sua mensagem" name="mensagem" required></textarea>
	  			</div>
	  			<div class="form-group pt-3">
	  				<button type="submit" class="btn btn-outline-secondary btn-scholae">enviar</button>
	  			</div>
	  		</form>
		</div>
		<div class="col-xs-12">
			<div id="map"></div>
		</div>
	</div>
</section>