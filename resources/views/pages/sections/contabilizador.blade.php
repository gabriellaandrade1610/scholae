<section id="contabilizador">
	<div class="col-12" id="counter">
		<div class="d-none d-lg-block">
			<div class="row justify-content-center">
		  		<div class="counter col-2 mt-5 mb-5 ml-3 mr-3">
				    <div class="col-8 mx-auto counter-img">  			
						<img src="/images/min/study.svg" class="img-fluid">
				    </div>  		
				    <div class="counter-text">  			
	  					<h3 class="card-title mt-2">+ de <span class="count" data-count="1000">0</span></h3>	
	  					<p>alunos impactados</p>
					</div> 
				</div> 
				<div class="counter col-2 mt-5 mb-5 ml-3 mr-3">
				    <div class="col-8 mx-auto counter-img">  			
						<img src="/images/min/treinamento.svg" class="img-fluid">
				    </div>  		
				    <div class="counter-text">  			
	  					<h3 class="card-title mt-2"><span class="count" data-count="30">0</span></h3>	
	  					<p>intervenções</p>
					</div>  
				</div>
				<div class="counter col-2 mt-5 mb-5 ml-3 mr-3">
				    <div class="col-8 mx-auto counter-img">  			
						<img src="/images/min/teachers.svg" class="img-fluid">
				    </div>  		
				    <div class="counter-text">  			
	  					<h3 class="card-title mt-2">+ de <span class="count" data-count="20">0</span></h3>	
	  					<p>professores impactados</p>
					</div>  
				</div>
				<div class="counter col-2 mt-5 mb-5 ml-3 mr-3">
				    <div class="col-8 mx-auto counter-img">  			
						<img src="/images/min/school.svg" class="img-fluid">
				    </div>  		
				    <div class="counter-text">  			
	  					<h3 class="card-title mt-2"><span class="count" data-count="5">0</span></h3>	
	  					<p> participando ativamente do programa</p>
					</div>  
				</div>
			</div>
		</div>
		<div class="d-block d-lg-none row justify-content-center">
			<div class="col-12 d-flex flex-wrap justify-content-between">
		  		<div class="counter col-6 mt-3">
				    <div class="col-8 mx-auto counter-img">  			
						<img src="/images/min/study.svg" class="img-fluid">
				    </div>  		
				    <div class="counter-text">  			
	  					<h3 class="card-title mt-2">+ de <span class="count" data-count="1000">0</span></h3>	
	  					<p>alunos impactados</p>
					</div> 
				</div> 
				<div class="counter col-6 mt-3">
				    <div class="col-8 mx-auto counter-img">  			
						<img src="/images/min/treinamento.svg" class="img-fluid">
				    </div>  		
				    <div class="counter-text">  			
	  					<h3 class="card-title mt-2"><span class="count" data-count="30">0</span></h3>	
	  					<p>intervenções</p>
					</div>  
				</div>
				<div class="counter col-6 mt-3">
				    <div class="col-8 mx-auto counter-img">  			
						<img src="/images/min/teachers.svg" class="img-fluid">
				    </div>  		
				    <div class="counter-text">  			
	  					<h3 class="card-title mt-2">+ de <span class="count" data-count="20">0</span></h3>	
	  					<p>professores impactados</p>
					</div>  
				</div>
				<div class="counter col-6 mt-3">
				    <div class="col-8 mx-auto counter-img">  			
						<img src="/images/min/school.svg" class="img-fluid">
				    </div>  		
				    <div class="counter-text">  			
	  					<h3 class="card-title mt-2"><span class="count" data-count="5">0</span></h3>	
	  					<p>escolas participando ativamente do programa</p>
					</div>  
				</div>
			</div>
		</div>
  	</div>
</section>