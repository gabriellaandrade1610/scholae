<section id="quem-somos" class="parallax-window" data-parallax="scroll" data-image-src="/images/min/background2-opacity.png">
	<div class="d-none d-lg-block">
	  	<div class="d-flex align-items-center mx-5">
		  	<div class="col-md-5 text-quem-somos m-5 pt-2">
		  		<h1 class="">Quem Somos</h1>
		  		<h4 class="pb-2"></h4>
		  		<p>Acreditamos que a escola deve ser o lugar onde os alunos despertem sua criatividade, deem vidas aos seus sonhos e construam o seu próprio futuro. Além disso, atuamos com abordagens de ensino capazes de estimular o pensamento empreendedor na construção de conhecimentos, projetos e propósitos de vida!</p>
		    	<a class="mt-1 mb-2 btn btn-outline-secondary btn-scholae anchor" data-target="#quem-somos" href="{{ route('index')}}'#quem-somos'" title="Quem somos">saiba mais</a>
			</div>
			<div class="col-md-5 m-5">
		  		<img src="/images/min/img-quem-somos.png" class="float-right img-fluid">
			</div>
		</div>
	</div>
	<div class="container d-block d-lg-none">
		<div class="text-quem-somos m-2 p-3">
			<div class="">
		  		<img src="/images/min/img-quem-somos.png" class="float-right img-fluid" width="400px">
			</div>
	  		<h1 class="">Quem Somos</h1>
	  		<h4 class="pb-2"></h4>
	  		<p>Acreditamos que a escola deve ser o lugar onde os alunos despertem sua criatividade, deem vidas aos seus sonhos e construam o seu próprio futuro. Além disso, atuamos com abordagens de ensino capazes de estimular o pensamento empreendedor na construção de conhecimentos, projetos e propósitos de vida!</p>
	    	<a class="mt-1 mb-2 btn btn-outline-secondary btn-scholae anchor" data-target="#quem-somos" href="{{ route('index')}}'#quem-somos'" title="Quem somos">saiba mais</a>
		</div>
	</div>
</section>
