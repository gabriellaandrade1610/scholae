@php $video = 'GF0iYHfTU7w' @endphp
<section id="oque-fazemos" class="oque-fazemos">
	  	<h1 class="text-center pt-5">O Que Fazemos ?</h1>
	  	<h4 class="text-center mb-2">Uma metodologia de trabalho baseada na construção, medição e aprendizado</h4>
	  	<div class="d-none  d-lg-block">
		  	<div class="d-flex align-items-center mx-5">
				<div class="col-md-5 m-5">
			  		<div class="video-scholae video" style="background-image: linear-gradient(270deg,rgba(0,0,0,.5) 0,rgba(0,0,0,.5)), url('https://img.youtube.com/vi/{{ $video }}/0.jpg');">
						<a class="fancybox" href="https://www.youtube.com/embed/{{ $video }}?autoplay=1&amp;rel=0&amp;controls=0" data-fancybox="gallery-video" style="background-image: url('images/min/icone-branco.png')">
							<img src="https://img.youtube.com/vi/{{ $video }}/0.jpg" alt="" class="sr-only">
						</a>
					</div>
				</div>
			  	<div class="col-md-5 m-5 pt-2">
			  		<p>Aplicamos oficinas em que os estudantes desenvolvem suas capacidades intelectuais de forma prática e interdisciplinar. Nossa metodologia está alinhada às competências da <a  href="http://basenacionalcomum.mec.gov.br/">BNCC</a>, logo, formamos os nossos alunos com pensamentos críticos e disruptivos impactando o mundo e a sociedade com suas criações.
					</p>
				</div>
			</div>
	  	</div>
	  	<div class="container d-block d-lg-none">
			<div class="col-xs-12 m-5">
		  		<div class="video-scholae video" style="background-image: linear-gradient(270deg,rgba(0,0,0,.5) 0,rgba(0,0,0,.5)), url('https://img.youtube.com/vi/{{ $video }}/0.jpg');">
					<a class="fancybox" href="https://www.youtube.com/embed/{{ $video }}?autoplay=1&amp;rel=0&amp;controls=0" data-fancybox="gallery-video" style="background-image: url('images/min/icone-branco.png')">
						<img src="https://img.youtube.com/vi/{{ $video }}/0.jpg" alt="" class="sr-only">
					</a>
				</div>
			</div>
		  	<div class="col-xs-12 m-5 pt-2 pb-2">
		  		<p>Aplicamos oficinas em que os estudantes desenvolvem suas capacidades intelectuais de forma prática e interdisciplinar. Nossa metodologia está alinhada às competências da <a  href="http://basenacionalcomum.mec.gov.br/">BNCC</a>, logo, formamos os nossos alunos com pensamentos críticos e disruptivos impactando o mundo e a sociedade com suas criações.
				</p>
			</div>
	  	</div>
</section>