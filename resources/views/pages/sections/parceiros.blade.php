<section id="parceiros">
	<div class="d-none  d-lg-block">
		<div class="d-flex align-items-center mx-5 p-5">
		  	<div class="col-md-5 mx-auto">
		  		<h1>Parceiros</h1>
			</div>
			<div class="col-md-5 mx-auto">
				<div class="slide-parceiros owl-carousel">
					<div class="item">
						<img src="{{ asset('/images/min/inpacta.png') }}" class="img-fluid">
					</div>
					<div class="item">
						<img src="{{ asset('/images/min/ufrn.png') }}" class="img-fluid">
					</div>
					<div class="item">
						<img src="{{ asset('/images/min/nossa-ciencia.png') }}" class="img-fluid">
					</div>
					<div class="item">
						<img src="{{ asset('/images/min/ijs-logo.png') }}" class="img-fluid">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container d-block d-lg-none">
		<div class="col-12">
	  		<h1 class="pt-5">Parceiros</h1>
		</div>
		<div class="col-12">
			<div class="slide-parceiros owl-carousel">
				<div class="item">
					<img src="{{ asset('/images/min/inpacta.png') }}" class="img-fluid">
				</div>
				<div class="item">
					<img src="{{ asset('/images/min/ufrn.png') }}" class="img-fluid">
				</div>
				<div class="item">
					<img src="{{ asset('/images/min/nossa-ciencia.png') }}" class="img-fluid">
				</div>
				<div class="item">
					<img src="{{ asset('/images/min/ijs-logo.png') }}" class="img-fluid">
				</div>
			</div>
		</div>
	</div>
</section>