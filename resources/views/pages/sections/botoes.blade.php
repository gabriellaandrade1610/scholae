<section id="botoes">
	<div class="container">
		<div class="row">
			{{-- <h1 class="text-center pt-5">UMA METODOLOGIA DE TRABALHO BASEADA NA CONSTRUÇÃO MEDIÇÃO E APRENDIZADO</h1> --}}
			<div class="mt-5 diviser"></div>
		</div>
	</div>

	<div class="d-none col-12 d-lg-block">
		<div class="row justify-content-center">
	  		<div class="flip-container col-2 m-5" ontouchstart="this.classList.toggle('hover');">  	
				<div class="flipper">  		
				    <div class="front">  			
						<img src="/images/min/computer.png" class="img-fluid">
	  					<h5 class="card-title mt-5">Tecnologia</h5>	
				    </div>  		
				    <div class="back">  			
						<h5 class="card-title">Tecnologia</h5>
	  					<p>utilizamos impressões 3D e robótica em nossas oficinas</p>		
				    </div>  	
				</div>  
			</div>
	  		<div class="flip-container col-2 m-5" ontouchstart="this.classList.toggle('hover');">  	
				<div class="flipper">  
					<div class="front">
	  					<img src="/images/min/smile.png" class="img-fluid">
	  					<h5 class="card-title mt-5">Competências emocionais</h5>
	  				</div>  		
	  				<div class="back">
	  					<h5 class="card-title">Competências emocionais</h5>  
	  					<p>o trabalho em equipe, a empatia, a comunicação são pilares fundamentais para o crescimento pessoal e profissional.</p>
	  				</div>
	  			</div> 
	  		</div>
	  		<div class="flip-container col-2 m-5" ontouchstart="this.classList.toggle('hover');">  	
				<div class="flipper"> 
				 	<div class="front">
	  					<img src="/images/min/idea.png" class="img-fluid">
	  					<h5 class="card-title mt-5">Mudança de pensamento</h5>
	  				</div>  		
	  				<div class="back">
	  					<h5 class="card-title">Mudança de pensamento</h5>
	  					<p>despertando criações sem barreiras</p>
	  				</div>
	  			</div>
	  		</div>
	  		<div class="flip-container col-2 m-5" ontouchstart="this.classList.toggle('hover');">  	
				<div class="flipper">  
					<div class="front">
		  				<img src="/images/min/project-management.png" class="img-fluid">
		  				<h5 class="card-title mt-5">Ambientes de inovação</h5>
	  				</div>  		
	  				<div class="back">
	  					<h5 class="card-title">Ambientes de inovação</h5>
	  					<p>oficinas em ambientes reais, possibilitando novas crenças e quebra de paradigmas.</p>
	  				</div>
	  			</div>
	  		</div>
		</div>
  	</div>
  	<div class="d-block col-12 d-lg-none">
		<div class="slide-botoes owl-carousel">
	  		<div class="flip-container col-12 m-5" ontouchstart="this.classList.toggle('hover');">  	
				<div class="flipper">  		
				    <div class="front">  			
						<img src="/images/min/computer.png" class="img-fluid">
	  					<h5 class="card-title mt-5">Tecnologia</h5>	
				    </div>  		
				    <div class="back">  			
						<h5 class="card-title">Tecnologia</h5>
	  					<p>utilizamos impressões 3D e robótica em nossas oficinas</p>		
				    </div>  	
				</div>  
			</div>
	  		<div class="flip-container col-12 m-5" ontouchstart="this.classList.toggle('hover');">  	
				<div class="flipper">  
					<div class="front">
	  					<img src="/images/min/smile.png" class="img-fluid">
	  					<h5 class="card-title mt-5">Competências emocionais</h5>
	  				</div>  		
	  				<div class="back">
	  					<h5 class="card-title">Competências emocionais</h5>  
	  					<p>o trabalho em equipe, a empatia, a comunicação são pilares fundamentais para o crescimento pessoal e profissional.</p>
	  				</div>
	  			</div> 
	  		</div>
	  		<div class="flip-container col-12 m-5" ontouchstart="this.classList.toggle('hover');">  	
				<div class="flipper"> 
				 	<div class="front">
	  					<img src="/images/min/idea.png" class="img-fluid">
	  					<h5 class="card-title mt-5">Mudança de pensamento</h5>
	  				</div>  		
	  				<div class="back">
	  					<h5 class="card-title">Mudança de pensamento</h5>
	  					<p>despertando criações sem barreiras</p>
	  				</div>
	  			</div>
	  		</div>
	  		<div class="flip-container col-12 m-5" ontouchstart="this.classList.toggle('hover');">  	
				<div class="flipper">  
					<div class="front">
		  				<img src="/images/min/project-management.png" class="img-fluid">
		  				<h5 class="card-title mt-5">Ambientes de inovação</h5>
	  				</div>  		
	  				<div class="back">
	  					<h5 class="card-title">Ambientes de inovação</h5>
	  					<p>oficinas em ambientes reais, possibilitando novas crenças e quebra de paradigmas.</p>
	  				</div>
	  			</div>
	  		</div>
		</div>
  	</div>
  	<div class="container">
		<div class="row">
			{{-- <h1 class="text-center pt-5">UMA METODOLOGIA DE TRABALHO BASEADA NA CONSTRUÇÃO MEDIÇÃO E APRENDIZADO</h1> --}}
			<div class="mt-5 diviser mb-5"></div>
		</div>
	</div>
</section>