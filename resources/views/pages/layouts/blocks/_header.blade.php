 <nav class="site-header sticky-top py-1">
 	<div class="container-fluid ">
 		<div class="row">
			<div class="col-10 d-flex flex-column flex-md-row justify-content-between align-items-center mx-auto">
				<a class="py-2" href="#">
				  <img src="/images/min/scholae-logo-2.png" class="img-fluid" width="100">
				</a>
				<a class="py-2 d-none d-md-inline-block anchor" data-target="#quem-somos" href="{{ route('index')}}'#quem-somos'" title="Quem somos">quem somos</a>
				<a class="py-2 d-none d-md-inline-block anchor" data-target="#oque-fazemos" href="{{ route('index')}}'#oque-fazemos'" title="O que fazemos">o que fazemos</a>
				<a class="py-2 d-none d-md-inline-block anchor" data-target="#metodologia" href="{{ route('index')}}'#metodologia'" title="Metodologia">metodologia</a>
				<a class="py-2 d-none d-md-inline-block btn-scholae anchor" data-target="#fale-conosco" href="{{ route('index')}}'#fale-conosco'">entre em contato</a>
			</div>
 		</div>
 	</div>
</nav>