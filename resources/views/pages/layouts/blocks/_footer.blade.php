<section id="footer">
  <div class="d-none  d-lg-block">
  	<footer class="container-fluid py-5">
        <div class="row">
          <div class="col-12 text-center">
            <!-- <div class="col-4 col-md d-flex flex-column align-items-center mx-auto">
              <video autoplay muted loop id="myVideo" style="width: 100%">
                <source src="video/video_dos_slides_mp4.mp4" type="video/mp4">
              </video>
              <small class="d-block mb-3 text-muted">&copy; 2017-2018</small>
            </div> -->
            <!-- <div class="col-3 col-md">
              <h5>Acesso Rápido</h5>
              <ul class="list-unstyled text-small">
                <li><a class="text-muted" href="#">Quem Somos</a></li>
                <li><a class="text-muted" href="#">O que fazemos</a></li>
                <li><a class="text-muted" href="#">Contato</a></li>
              </ul>
            </div> -->
            <!-- <div class="col-5 col-md">
              <h5>Endereço</h5>
              <ul class="list-unstyled text-small">
                <li><a class="text-muted" href="https://www.google.com/maps/place/inPACTA/@-5.8459007,-35.2050604,17z/data=!3m1!4b1!4m5!3m4!1s0x7b2ff70bb0615b3:0xc9620339535fdddc!8m2!3d-5.8459007!4d-35.2028717">
                	<i class="fa fa-map-marker"></i> R. Monte Sinai, 1853 - Capim Macio, Natal - RN, 59078-360</a>
                </li>
                <li><a class="text-muted" href="tel:999999999">
                	<i class="fa fa-phone"></i> (84) 9 8733.8651</a></li>
                <li><a class="text-muted" href="mail:scholae@gmail.com">
                	<i class="fa fa-envelope"></i> contato@scholae30.com.br</a>
                </li>
              </ul>
            </div> -->
            <p class="text-center color-gray">© 2018 - 2019 - Scholae 3.0 - Todos os Direitos Reservados.</p>
            <h3 class="text-center color-gray">Siga nossas redes sociais</h3>
            <!-- <a class="text-muted" href="mail:scholae@gmail.com">
                  <i class="fa fa-facebook"></i>
            </a> -->
            <a class="text-muted" href="https://www.instagram.com/scholae3.0/">
                  <i class="fa fa-instagram"></i>
            </a>
          </div>
        </div>
    </footer>
  </div>
  <div class="container d-block d-lg-none">
    <footer>
        <div class="col-12 d-flex flex-column flex-md-row justify-content-between align-items-center mx-auto">
          <video autoplay muted loop id="myVideo" style="width: 100%">
            <source src="video/video_dos_slides_mp4.mp4" type="video/mp4">
          </video>
          <small class="d-block mb-3 text-muted">&copy; 2017-2019</small>
        </div>
        <div class="col-12 d-flex flex-wrap justify-content-between">
          <div class="col-6">
            <h5>Acesso Rápido</h5>
            <ul class="list-unstyled text-small">
              <li><a class="text-muted" href="#">Quem Somos</a></li>
              <li><a class="text-muted" href="#">O que fazemos</a></li>
              <li><a class="text-muted" href="#">Contato</a></li>
            </ul>
          </div>
          <div class="col-6">
            <h5>Endereço</h5>
            <ul class="list-unstyled text-small">
              <li><a class="text-muted" href="https://www.google.com/maps/place/inPACTA/@-5.8459007,-35.2050604,17z/data=!3m1!4b1!4m5!3m4!1s0x7b2ff70bb0615b3:0xc9620339535fdddc!8m2!3d-5.8459007!4d-35.2028717">
                <i class="fa fa-map-marker"></i> R. Monte Sinai, 1853 - Capim Macio, Natal - RN, 59078-360</a>
              </li>
              <li><a class="text-muted" href="tel:999999999">
                <i class="fa fa-phone"></i> (84) 9 8733.8651</a></li>
              <li><a class="text-muted" href="mail:scholae@gmail.com">
                <i class="fa fa-envelope"></i> contato@scholae30.com.br</a>
              </li>
            </ul>
          </div>
        </div>
    </footer>
  </div>
</section>