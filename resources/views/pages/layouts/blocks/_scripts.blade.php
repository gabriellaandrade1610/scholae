<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<script src="/js/dist/app.js"></script>
<script src="/js/parallax.min.js"></script>
<script src="https://unpkg.com/leaflet@1.3.4/dist/leaflet.js"></script>

@include('pages.layouts.blocks._swal')

{{-- SE NECESSÁRIO UTILIZAR UM JS ESPECÍFICO PARA ALGUMA PÁGINA --}}
@yield('js')

<script type="text/javascript">
	var mymap = L.map('map').setView([-5.84702,-35.203735], 16);
	L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {}).addTo(mymap);
	var marker = L.marker([-5.84702,-35.203735]).addTo(mymap);
	marker.bindPopup("<b>Scholae 3.0</b>").openPopup();
</script>